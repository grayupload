/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <grayupload.h>

int
copy_file (char const *wdname, int wd, char const *infile)
{
  FILE *ifp, *ofp;
  int fd;
  char const *bname;
  int c;
  int rc;
  
  ifp = fopen (infile, "r");
  if (!ifp)
    {
      error ("can't open file %s: %s", infile, strerror (errno));
      return -1;
    }

  bname = basename (infile);

  if (verbose)
    debug ("copying %s to %s/%s", infile, wdname, bname);
  
  fd = openat (wd, bname, O_CREAT | O_WRONLY | O_TRUNC, 0644);
  if (fd == -1)
    {
      error ("can't create file %s/%s: %s", wdname, bname, strerror (errno));
      fclose (ifp);
      return -1;
    }

  ofp = fdopen (fd, "w");
  if (!ofp)
    {
      error ("fdopen failed: %s", strerror (errno));
      close (fd);
      fclose (ifp);
      return -1;
    }

  while ((c = fgetc (ifp)) != EOF)
    fputc (c, ofp);

  if (ferror (ifp))
    {
      error ("read error on %s: %s", infile, strerror (errno));
      rc = -1;
    }
  else if (ferror (ofp))
    {
      error ("write error on %s/%s: %s", wdname, bname, strerror (errno));
      rc = -1;
    }
  else
    rc = 0;
  
  fclose (ifp);
  fclose (ofp);
  return rc;
}

int
file_upload (struct destination *dest, int argc, char **argv)
{
  int fd;
  int i;
  int rc;
  
  fd = open (dest->upload_dir, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
  if (fd == -1)
    {
      error ("can't open upload directory %s: %s",
	     dest->upload_dir, strerror (errno));
      exit (EX_FAILURE);
    }
  rc = 0;
  for (i = 0; i < argc; i++)
    if ((rc = copy_file (dest->upload_dir, fd, argv[i])) != 0)
      break;
  close (fd);
  return rc;
}
