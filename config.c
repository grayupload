/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <grayupload.h>

static int
is_kwchar (int c)
{
  return isalnum (c) || c == '_';
}

static int
is_kwstart (int c)
{
  return isalpha (c) || c == '_';
}

static struct conf_keyword *
find_keyword (char const *name, struct conf_keyword *kwtab)
{
  for (; kwtab->name; kwtab++)
    {
      if (strcmp (kwtab->name, name) == 0)
	return kwtab;
    }
  return NULL;
}

int
config_parse (char const *file_name, struct conf_keyword *kwtab, int okmissing)
{
  FILE *fp;
  struct conf_locus locus;
  char buf[512], *p;
  int nerr = 0;

  if (verbose)
    debug ("reading configuration file %s", file_name);
  fp = fopen (file_name, "r");
  if (fp == NULL)
    {
      if (okmissing && errno == ENOENT)
	return 0;
      error ("can't open file %s: %s", file_name, strerror (errno));
      exit (EX_FAILURE);
    }

  locus.file = file_name;
  locus.line = 0;

 restart:
  while ((p = fgets (buf, sizeof (buf), fp)) != NULL)
    {
      int i, len;
      char *kwname;
      struct conf_keyword *kw;

      locus.line++;

      len = strlen (buf);

      /* Remove trailing newline */
      if (len > 0 && buf[len-1] != '\n')
	{
	  int c;
	  error_at_locus (&locus, "line too long: skipped");
	  nerr++;
	  while ((c = fgetc (fp)) != '\n')
	    {
	      if (c == EOF)
		goto end;
	    }
	  continue;
	}

      /* Remove eventual comment */
      for (i = len-1; i >= 0; i--)
	{
	  if (buf[i] == '"' || buf[i] == '#')
	    break;
	}

      if (i >= 0 && buf[i] == '#')
	{
	  len = i;
	}

      /* Remove trailing whitespace */
      while (len > 0 && isspace (buf[len-1]))
	--len;
      buf[len] = 0;

      /* Remove initial whitespace */
      while (len > 0 && isspace (*p))
	{
	  p++;
	  len--;
	}

      /* Skip emtpy and comment lines. */
      if (len == 0 || *p == '#')
	continue;

      /* Separate keyword and value. */
      if (!is_kwstart (*p))
	{
	  error_at_locus (&locus, "unrecognized line");
	  nerr++;
	  continue;
	}
      kwname = p;
      for (; !isspace (*p); p++)
	{
	  if (!is_kwchar (*p))
	    {
	      error_at_locus (&locus, "unrecognized line");
	      nerr++;
	      goto restart;
	    }
	}

      *p++ = 0;
      len -= p - kwname;

      /* Skip whitespace */
      while (len > 0 && isspace (*p))
	{
	  p++;
	  len--;
	}

      /* Remove quotes */
      if (*p == '"' && p[len-1] == '"')
	{
	  char *q, *r;

	  p++;
	  len--;
	  p[len-1] = 0;
	  q = r = p;
	  do
	    {
	      if (*q == '\\')
		q++;
	    }
	  while ((*r++ = *q++) != 0);
	}
      else
	p[len] = 0;

      if (*p == 0)
	{
	  error_at_locus (&locus, "malformed line: missing value");
	  nerr++;
	  continue;
	}

      kw = find_keyword (kwname, kwtab);
      if (!kw)
	{
	  error_at_locus (&locus, "unknown keyword");
	  nerr++;
	  continue;
	}

      if (kw->set (p, kw, &locus))
	nerr++;
    }
 end:
  if (ferror (fp))
    {
      error_at_locus (&locus, "file read error");
      nerr++;
    }
  fclose (fp);
  return nerr;
}
