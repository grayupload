/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
void *xmalloc (size_t size);
void *xcalloc (size_t nmemb, size_t size);
void *xrealloc (void *ptr, size_t size);
char *xstrdup (char const *s);
void *x2nrealloc (void *p, size_t *pn, size_t s);

/*
 * Singly-linked list macros.
 */

#define SLIST_HEAD(name, type)			\
  struct name					\
    {						\
      struct type *sl_first;			\
      struct type *sl_last;			\
    }

#define SLIST_HEAD_INITIALIZER(head)		\
  { NULL, NULL }

#define SLIST_ENTRY(type)			\
  struct type *

#define SLIST_INIT(head)				\
  do							\
    {							\
      (head)->sl_first = (head)->sl_last = NULL;	\
    }							\
  while (0)

/* Insert elt at the head of the list. */
#define SLIST_INSERT_HEAD(elt, head, field)			\
  do								\
    {								\
      if (((elt)->field = (head)->sl_first) == NULL)		\
	(head)->sl_last = (elt);				\
      (head)->sl_first = (elt);					\
    }								\
  while (0)

/* Append elt to the tail of the list. */
#define SLIST_INSERT_TAIL(head, elt, field)		\
  do							\
    {							\
      if ((head)->sl_last)				\
	(head)->sl_last->field = (elt);			\
      else						\
	(head)->sl_first = (elt);			\
      (head)->sl_last = (elt);				\
      (elt)->field = NULL;				\
    }							\
  while (0)

#define SLIST_PUSH   SLIST_INSERT_TAIL

/* Insert elt after given element (anchor). */
#define SLIST_INSERT_AFTER(head, anchor, elt, field)		\
  do								\
    {								\
      if (((elt)->field = (anchor)->field) == NULL)		\
	(head)->sl_last = (elt);				\
      (anchor)->field = (elt);					\
    }								\
  while (0)

/* Remove element from the head of the list. */
#define SLIST_REMOVE_HEAD(head, field)					\
  do									\
    {									\
      if ((head)->sl_first != NULL &&					\
	  ((head)->sl_first = (head)->sl_first->field) == NULL)		\
	(head)->sl_last = NULL;						\
    }									\
  while (0)

#define SLIST_SHIFT  SLIST_REMOVE_HEAD

#define SLIST_FOREACH(var, head, field)		\
  for ((var) = (head)->sl_first; (var); (var) = (var)->field)

#define SLIST_COPY(dst, src)			\
  *dst = *src

#define SLIST_CONCAT(a, b, field)				\
  do								\
    {								\
      if ((a)->sl_last)						\
	(a)->sl_last->field = (b)->sl_first;			\
      else							\
	(a)->sl_first = (b)->sl_first;				\
      (a)->sl_last = (b)->sl_last;				\
    }								\
  while (0)

#define SLIST_FIRST(head) ((head)->sl_first)
#define SLIST_LAST(head) ((head)->sl_last)
#define SLIST_EMPTY(head) (SLIST_FIRST (head) == NULL)
#define SLIST_NEXT(elt, field) ((elt)->field)

#define OPTFLAG_DEFAULT      0
#define OPTFLAG_ALIAS        0x1
#define OPTFLAG_DOC          0x2
#define OPTFLAG_ARG_OPTIONAL 0x4
#define OPTFLAG_HIDDEN       0x8

struct optdef
{
  char *opt_name;
  char *opt_argdoc;
  int opt_flags;
  int opt_code;
  void *opt_doc;
};

struct parseopt
{
  /* User-supplied data: */
  char *descr;           /* Program description. */
  char *argdoc;          /* Argument docstring. */
  struct optdef *optdef; /* Option definitions. */
  char *extradoc;        /* Extra documentation. */

  /* Internal fields: */
  int *optidx;           /* Option indexes in lexicographical order. */
  int optcount;          /* Number of elements in optidx (actual options). */

  /* Fields initialized by parseopt_init: */
  int argc;              /* Number of arguments. */
  char **argv;           /* Argument vector. */

  /* Fields initialized by parseopt_init and maintained by parseopt_next: */
  int argi;              /* Index of next argument in argv[]. */
  int eopt;              /* End of options: if true, treat remaining argv as
			    filename arguments. */
  char const *optname;   /* Current option. */
  int dash_count;        /* Is it a long option? */
  char opt_buf[2];
};

int parseopt_next (struct parseopt *po, char **ret_arg);
int parseopt_init (struct parseopt *po, int argc, char **argv);
void usage (int err, struct parseopt *po);

extern const char *progname;

struct conf_locus
{
  char const *file;
  int line;
};

struct conf_keyword;
typedef int (*conf_setter) (char const *, struct conf_keyword *,
			    struct conf_locus *);

struct conf_keyword
{
  char *name;
  conf_setter set;
  void *dest;
};

void error_at_locus (struct conf_locus *locus, char const *fmt, ...);
int config_parse (char const *file_name, struct conf_keyword *kwtab,
		  int okmissing);

typedef SLIST_HEAD (, string_entry) STRING_LIST;
struct string_entry
{
  SLIST_ENTRY (string_entry) next;
  char *string;
};

struct string_entry *string_entry_new (char const *s, size_t n);
void string_list_append (STRING_LIST *slist, char const *s, size_t n);
char *string_list_coalesce (STRING_LIST *slist, char **pbuf, size_t *psize);
void string_list_free (STRING_LIST *slist);

typedef SLIST_HEAD (transform_list, transform_entry) *TRANSFORM;

TRANSFORM compile_transform_expr (const char *expr, int cflags);
char *transform_string (TRANSFORM tf, const char *input);

struct destination
{
  char *release_type;
  char *url;
  char *hostname;
  unsigned int port;
  char *username;
  char *upload_dir;
  char *directory;
  int (*upload) (struct destination *, int, char **);
  SLIST_ENTRY (destination) next;
};

void error (char const *fmt, ...);
void warning(char const *fmt, ...);
void debug (char const *fmt, ...);

char const *basename (char const *file);
char *catname (char const *dir, char const *file);

int getyn (const char *prompt, ...);

int file_upload (struct destination *dp, int argc, char **argv);
int sftp_upload (struct destination *dp, int argc, char **argv);
int ftp_upload (struct destination *dp, int argc, char **argv);

enum
{
  EX_OK,
  EX_FAILURE,
  EX_USAGE,
  EX_CONFIG,
};

extern int verbose;
extern int dry_run;
