.\" This file is part of Grayupload -*- nroff -*-
.\" Copyright (C) 2023-2024 Sergey Poznyakoff
.\"
.\" Grayupload is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Grayupload is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Grayupload.  If not, see <http://www.gnu.org/licenses/>.
.TH GRAYUPLOAD 1 "July 21, 2024" "GRAYUPLOAD" "User Commands"
.SH NAME
grayupload \- distribute tarballs via anonymous FTP upload
.SH SYNOPSIS
\fBgrayupload\fR\
 [\fB\-?Vnv\fR]\
 [\fB\-t\fR \fITYPE\fR]\
 [\fB\-u\fR \fINAME\fR]\
 [\fB\-\-url \fISCHEME\fB://[\fIUSER\fB@\fR][\fIHOST\fR]\fB/\fR[\fIUPDIR\fR][\fB;\fIDESTDIR\fR]\
 [\fB\-\-to \fIHOST\fB:\fIDIR\fR]\
 [\fB\-\-directory \fIDIR\fR]\
 [\fB\-\-config \fIFILE\fR]\
 [\fB\-\-no\-config\fR]\
 [\fB\-\-dry\-run\fR]\
 [\fB\-\-user \fINAME\fR]\
 [\fB\-\-verbose\fR]\
 [\fB\-\-comment \fITEXT\fR]\
 [\fB\-\-delete \fIFILE\fR...]\
 [\fB\-\-symlink \fIFILE\fR \fISYMLINK\fR...]\
 [\fB\-\-release\-type \fINAME\fR]\
 [\fB\-\-upload \fIFILE\fR...]\
 [\fB\-\-replace\fR]\
 [\fB\-\-latest\fR]\
 [\fB\-\-transform\-symlink \fIS\-EXP\fR]\
 [\fB\-\-help\fR]\
 [\fB\-\-version\fR]\
 [\fIFILE\fR...]
.SH DESCRIPTION
Upload software tarballs to distribution sites using the
.IR "Anonymous FTP Upload Protocol" .
.PP
This manpage is a short description of \fBgrayupload\fR.  For a detailed
discussion, including examples and usage recommendations, refer to the
\fBGrayupload User Manual\fR, available in texinfo format.  If the
\fBinfo\fR reader and \fBgrayupload\fR documentation are properly
installed on your system, the command
.PP
.RS +4
.B info grayupload
.RE
.PP
should give you access to the complete manual.
.PP
You can also view the manual using the info mode in
.BR emacs (1),
or find it in various formats online at
.PP
.RS +4
.B https://www.gnu.org.ua/software/grayupload
.RE
.PP
If any discrepancies occur between this manpage and the
\fBGrayupload User Manual\fR, the later shall be considered the
authoritative source.
.SH OPTIONS
.SS Destination Selection Options
.TP
\fB\-\-url \fISCHEME\fB://[\fIUSER\fB@\fR][\fIHOST\fR]\fB/\fR[\fIUPDIR\fR][\fB;\fIDESTDIR\fR]
.RS
.TP
.I SCHEME
Defines the protocol used for upload.  Following values are
understood: \fBftp\fR, for anonymous FTP upload, \fBsftp\fR, for upload
via SFTP, and \fBfile\fR, for copying triplet files to a local
directory.  In the latter case, both \fIUSER\fR and \fIHOST\fR parts
must be omitted.
.TP
.I USER
Optional username for authorization.  Used for \fBftp\fR and
\fBsftp\fR schemes.  If not supplied, \fBftp\fR is used for \fBftp\fR
scheme, and current login name for \fBsftp\fR.
.TP
.I HOST
Hostname of the upload server.
.TP
.I UPDIR
Upload directory.
.TP
.I DESTDIR
Destination directory.
.RE
.TP
\fB\-\-to \fIHOST\fB:\fIDIR\fR
.RS
.TP
.BI alpha.gnu.org: DIRECTORY
Upload for distribution from <https://alpha.gnu.org/gnu/\fIDIRECTORY\fR>.
.TP
.BI ftp.gnu.org: DIRECTORY
Upload for distribution from <https://ftp.gnu.org/gnu/\fIDIRECTORY\fR>.
.TP
[\fIUSER\fB@]\fBdownload.gnu.org.ua:alpha/\fIDIRECTORY\fR
Upload for distribution from <https://download.gnu.org.ua/alpha/\fIDIRECTORY\fR>.
If your remote and local user names differ, use the \fIUSER\fB@\fR prefix.
.TP
[\fIUSER\fB@]\fBdownload.gnu.org.ua:ftp/\fIDIRECTORY\fR
Upload for distribution from <https://download.gnu.org.ua/release/\fIDIRECTORY\fR>.
.TP
\fB/\fIDIRECTORY\fR[\fB;\fIDESTDIR\fR]
Prepare the triplet for distribution from \fIDESTDIR\fR and copy it to
the local directory \fIDIRECTORY\fR.  If \fIDESTDIR\fR is omitted,
\fIDIRECTORY\fR is used instead.
.RE
.TP
\fB\-\-directory \fIDIR\fR
Set destination directory name.  This name will be used for any
subsequent
.B \-\-url
options that doesn't specify destination directory explicitly.
.TP
\fB\-t\fR, \fB\-\-release\-type \fITYPE\fR.
Select only destinations marked with this release type.
.IP
To mark a destination for use with a particular release type, prefix
it with \fITYPE=\fR in its \fBto\fR or \fBurl\fR definition in the
configuration file.
.IP
See
.BR CONFIGURATION ,
for a detailed discussion.
.SS General-Purpose Options
.TP
\fB\-n\fR, \fB\-\-dry\-run\fR
Enables
.I dry-run
mode: don't upload any files, just print what would have been done.
Use additional
.I \-v
options to increase debugging verbosity.
.TP
\fB\-u, \fB\-\-user \fIID\fR
Sign tarballs and directives with the GPG key
.IR id .
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Increase debugging verbosity level.  The option is incremental: the
more times you repeat it the more verbosity you get.
.TP
\fB\-\-comment \fITEXT\fR
Add comment \fITEXT\fR to the directive.  By default, single comment
is added specifying the name and version of the program that did the
upload.  Multiple \fB\-\-comment\fR options are allowed.
.TP
\fB\-\-config \fIFILE\fR
Read configuration from \fIFILE\fR.  The file must exist and be
readable.  See the section
.BR CONFIGURATION ,
for details.
.TP
\fB\-\-no\-config\fR
Don't read configuration file.
.SS File Selection Options
All options discussed in this subsection are standalone and are 
submitted in a separate directive file. Default is \fB\-\-upload\fR.  Use it 
explicitly, if you submit both standalone directive and upload files in one 
go.  Note, that in this case the remote server gives no promise on the exact 
order in which the submitted directives will be applied.
.TP
\fB\-\-delete \fIFILE\fR...
Delete these files from the remote server.  One or more arguments are allowed.
.TP
\fB\-\-replace\fR
If a tarball already exists on the distribution server, replace it with
the supplied one.
.TP
\fB\-\-rmsymlink\fR \fIFILE\fR...
Remove listed symlinks.
.TP
\fB\-\-symlink \fIFILE\fR \fISYMLINK\fR...
Delete these symbolic links from the remote server.  One or more
arguments are allowed.
.TP
\fB\-\-upload \fIFILE\fR...
Upload files to the remote server.  This is the default.  Use this
option after any of the above options to mark end of arguments to the
previous option, and start of file names for upload.
.SS Symlink Creation Options
.TP
\fB\-\-latest\fR
For each file uploaded to the server, create the \fIlatest\fR symlink.
The name of the symlink is obtained by replacing version number in the
file name with the word \fI\-latest\fR.  For example, the link name
for the archive \fBfoo\-1.0.tar.gz\fR will be \fBfoo\-latest.tar.gz\fR.
.TP
\fB\-\-symlink\-regex\fR[\fB=\fIS-EXP\fR]
This option is provided for compatibility with the \fBgnupload\fR
script.  It is equivalent to \fB\-\-transform\-symlink \fIS-EXP\fR,
if the argument is supplied, and to \fB\-\-latest\fR otherwise.
.IP
Notice, that the argument, if present, must be delimited from the
option name by a single equals sign with no additional whitespace
on either side of it.
.TP
\fB\-\-transform\-symlink \fIS\-EXP\fR
For each uploaded file, create a symlink, whose name is obtained by
applying a @command{sed} expression \fIS\-EXP\fR to the file name.
.SS Informative Options
.TP
\fB\-?\fR, \fB\-\-help\fR
Display a short usage summary and exit.
.TP
\fB\-V\fR, \fB\-\-version\fR
Display program version and distribution license and exit.
.SH CONFIGURATION
Upon startup, the program checks if the file
.B .grayupload
exists in the current working directory, and sources it if so.
Alternative file location can be given using the
.B \-\-config
option. Reading of the configuration file can be suppressed by using
.B \-\-no\-config
command line option.
.PP
The file is processed line by line.  Empty lines and comments
(introduced with the \fB#\fR sign) are ignored.   Leading and
trailing whitespace is removed.  A valid configuration line consists
of a keyword and value separated with any amount of whitespace.
.PP
Keywords correspond to option names without leading dash:
.TP
\fBto \fIDEST\fR
Defines destination location.  \fIDEST\fR must be one of standard
destinations (see the description of the \fI\-\-to\fR option, above),
optionally prefixed with \fITYPE\fB=\fR, to indicate that it is to be
used only for the given release type (as given by the
\fB\-\-release\-type\fR command line option).
.TP
\fBurl \fIURL\fR
Defines destination URL. See the description of the \fI-url\fR option
above, for the syntax of \fIURL\fR.  \fIURL\fR may be prefixed with
\fITYPE\fB=\fR, to indicate that it is to be used only for the given
release type (as given by the \fB\-\-release\-type\fR command line option).
.TP
\fBdirectory \fINAME\fR
Defines default destination directory name.  This name
will be used for any subsequent \fBurl\fR statement that doesn't
specify destination directory explicitly.
.TP
\fBuser \fIID\fR
Sign tarballs and directives with the GPG key \fIID\fR.
.TP
\fBcomment \fITEXT\fR
Adds comment \fITEXT\fR to the directive file.  By default, single comment
is added specifying the name and version of the program that did the
upload.  Multiple
.B comment
statements are allowed.  They don't override, but rather append their
arguments to the list of comments created by these statements.
.TP
\fBverbose \fINUM\fR
Sets verbosity level.  Allowed values for \fINUM\fR are 0 through 3.
.TP
\fBlatest \fIBOOL\fR
If \fIBOOL\fR is \fBtrue\fR, then, for each file uploaded to the
server, create the "latest" symlink.  For example, the link name
for the archive \fBfoo\-1.0.tar.gz\fR will be
\fBfoo\-latest.tar.gz\fR.
.TP
\fBtransform_symlink \fIS-EXP\fR
For each uploaded file, create a symlink, with the
name obtained by applying a \fBsed\fR expression \fIS-EXP\fR
to the file name.
.TP
\fBreplace \fIBOOL\fR
If set to \fBtrue\fR, when a tarball already exists on the
distribution server, replace it with the supplied one.
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2023--2024 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
