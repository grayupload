## Makefile for the grayupload documentation.
## Copyright (C) 2022-2024 Sergey Poznyakoff
##
## Grayupload is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by the
## Free Software Foundation; either version 3 of the License, or (at your
## option) any later version.
##
## Grayupload is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with grayupload. If not, see <http://www.gnu.org/licenses/>. */
dist_man_MANS   = grayupload.1
info_TEXINFOS   = grayupload.texi
grayupload_TEXINFOS = fdl.texi

GENDOCS = perl gendocs.pl
EXTRA_DIST = gendocs.pl webdoc.init otherdoc.texi.in

# Make sure you set TEXINPUTS.
# TEXINPUTS=/usr/share/texmf/pdftex/plain/misc/ is ok for most distributions
.PHONY: manual
manual:
	rm -rf manual
	TEXINPUTS=$(srcdir):$(top_srcdir)/build-aux:$(TEXINPUTS) \
	 MAKEINFO="$(MAKEINFO) $(AM_MAKEINFOFLAGS) $(MAKEINFOFLAGS)" \
	 $(GENDOCS) -C manual -o otherdoc.texi $(PACKAGE) otherdoc.texi.in
	$(MAKEINFO) $(AM_MAKEINFOFLAGS) $(MAKEINFOFLAGS) -DWEBDOC \
	   --html --init-file=webdoc.init $(info_TEXINFOS) -o manual
