/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <setjmp.h>
#include <limits.h>
#include <grayupload.h>

struct ftp_connection
{
  FILE *ctl;
  char buf[BUFSIZ];
  unsigned code;
  struct destination *dest;
};

#ifndef NI_MAXHOST
# define NI_MAXHOST      1025
#endif

#ifndef NI_MAXSERV
# define NI_MAXSERV      32
#endif

#define MAX_ADDR_BUFSIZE (NI_MAXHOST + NI_MAXSERV + 4)

char *
addr2str (char *res, int res_len, const struct sockaddr *addr, socklen_t addrlen)
{
  int rc;
  char hostbuf[NI_MAXHOST];
  char portbuf[NI_MAXSERV];

  if (res == NULL || res_len <= 0 || addr == NULL)
    return NULL;

  res[res_len - 1] = 0;
  --res_len;

  rc = getnameinfo (addr, addrlen,
		    hostbuf, sizeof (hostbuf),
		    portbuf, sizeof (portbuf),
		    NI_NUMERICHOST | NI_NUMERICSERV);

  if (rc)
    {
      error ("getnameinfo: %s", gai_strerror (rc));
      strncpy (res, "(UNKNOWN)", res_len);
    }
  else
    {
      if (addr->sa_family == AF_INET6)
	snprintf (res, res_len, "[%s]:%s", hostbuf, portbuf);
      else
	snprintf (res, res_len, "%s:%s", hostbuf, portbuf);
    }
  return res;
}

struct ftp_connection *
ftp_connect (struct destination *dest)
{
  int rc;
  struct addrinfo *addr, *ap, hints;
  char *servptr;
  char servbuf[128];
  char addrbuf[MAX_ADDR_BUFSIZE];
  struct ftp_connection *fc;
  FILE *fp;
  int fd;

  memset (&hints, 0, sizeof (hints));
  hints.ai_protocol = IPPROTO_TCP;
  hints.ai_socktype = SOCK_STREAM;

  if (dest->port)
    {
      if (snprintf (servbuf, sizeof servbuf, "%u", dest->port) >= sizeof servbuf)
	{
	  error ("%s:%d: INTERNAL ERROR: formatted port number too long",
		 __FILE__, __LINE__);
	  exit (EX_FAILURE);
	}
      servptr = servbuf;
    }
  else
    servptr = "ftp";

  rc = getaddrinfo (dest->hostname, servptr, &hints, &addr);
  if (rc)
    {
      error ("can't resolve %s:%s: %s", dest->hostname, servptr,
	     gai_strerror (rc));
      exit (EX_FAILURE);
    }

  fd = -1;
  for (ap = addr; ap; ap = ap->ai_next)
    {
      if (verbose > 1)
	debug ("connecting to %s", addr2str (addrbuf, sizeof addrbuf,
					     ap->ai_addr, ap->ai_addrlen));
      fd = socket (ap->ai_family, ap->ai_socktype, 0);
      if (fd == -1)
	{
	  error ("socket(%d, %d): %s", ap->ai_family, ap->ai_socktype,
		 strerror (errno));
	  continue;
	}

      if (connect (fd, ap->ai_addr, ap->ai_addrlen))
	{
	  error ("can't connect to %s:%s",
		 addr2str (addrbuf, sizeof addrbuf,
			   ap->ai_addr, ap->ai_addrlen),
		 strerror (errno));
	  close (fd);
	  fd = -1;
	  continue;
	}
      break;
    }

  if (fd == -1)
    exit (EX_FAILURE);

  if (verbose)
    debug ("successfully connected to %s",
	   addr2str (addrbuf, sizeof addrbuf, ap->ai_addr, ap->ai_addrlen));
  freeaddrinfo (ap);

  if ((fp = fdopen (fd, "r+")) == NULL)
    {
      error ("fdopen: %s", strerror (errno));
      exit (EX_FAILURE);
    }

  fc = xcalloc (1, sizeof (fc[0]));
  fc->ctl = fp;
  fc->dest = dest;
  return fc;
}

static char const *
ftp_hostname (struct ftp_connection *fc)
{
  return fc->dest->hostname;
}

static void
ftp_disconnect (struct ftp_connection *fc)
{
  fclose (fc->ctl);
  free (fc);
}

static int
ftp_send_command (struct ftp_connection *fc, char const *fmt, ...)
{
  va_list ap;
  int rc;

  va_start (ap, fmt);
  rc = vsnprintf (fc->buf, sizeof fc->buf, fmt, ap);
  va_end (ap);

  if (rc >= sizeof (fc->buf))
    {
      error ("%s:%d: INTERNAL ERROR: formatted command too long",
	     __FILE__, __LINE__);
      exit (EX_FAILURE);
    }

  if (verbose > 2)
    debug ("C: %s", fc->buf);

  fprintf (fc->ctl, "%s\r\n", fc->buf);

  if (ferror (fc->ctl))
    {
      error ("error sending command to %s: %s",
	     ftp_hostname (fc),
	     strerror (errno));
      return -1;
    }
  return 0;
}

static int
ftp_read_response (struct ftp_connection *fc)
{
  size_t len;
  char *p;
  unsigned long n;

  if (fgets (fc->buf, sizeof fc->buf, fc->ctl) == NULL)
    {
      error ("error reading response from %s: %s",
	     ftp_hostname (fc), strerror (errno));
      return -1;
    }

  len = strlen (fc->buf);
  if (len < 2 || fc->buf[len-1] != '\n')
    {
      error ("truncated response from %s", ftp_hostname (fc));
      return -1;
    }

  fc->buf[len-2] = 0;
  if (verbose > 2)
    debug ("S: %s", fc->buf);

  errno = 0;
  n = strtoul (fc->buf, &p, 10);
  if (errno || p - fc->buf != 3 || !(*p == 0 || *p == ' '))
    {
      error ("malformed response from %s: %s",
	     ftp_hostname (fc), strerror (errno));
      return -1;
    }
  fc->code = n;

  return 0;
}

static void
ftp_close (struct ftp_connection *fc)
{
  ftp_send_command (fc, "%s", "QUIT");
  ftp_read_response (fc);
  ftp_disconnect (fc);
}

static int
ftp_auth (struct ftp_connection *fc)
{
  if (ftp_send_command (fc, "USER %s",
			fc->dest->username ? fc->dest->username : "ftp"))
    return -1;
  if (ftp_read_response (fc))
    return -1;
  if (fc->code == 331)
    {
      //FIXME: password
      if (ftp_send_command (fc, "PASS %s@", fc->dest->username ? fc->dest->username : "ftp"))
	return -1;
      if (ftp_read_response (fc))
	return -1;
    }

  if (fc->code != 230)
    {
      error ("authentication failed: %s", fc->buf);
      return -1;
    }
  return 0;
}

static int
ftp_parse_pasv_response (struct ftp_connection *fc, struct sockaddr_in *s_in)
{
  /* 227 Entering Passive Mode (h1,h2,h3,h4,p1,p2). */
  int fd;
  char *p;
  in_addr_t ip;
  unsigned short port;
  unsigned int n;

#define GETBYTE(v, c)					\
  do							\
    {							\
      errno = 0;					\
      n = strtoul (p, &p, 10);				\
      if (errno || *p != c || n > UCHAR_MAX)		\
	goto err;					\
      p++;						\
      v = (v << 8) | n;					\
    }							\
  while (0)

  if ((p = strrchr (fc->buf, '(')) == NULL)
    goto err;
  p++;

  ip = 0;
  GETBYTE (ip, ',');
  GETBYTE (ip, ',');
  GETBYTE (ip, ',');
  GETBYTE (ip, ',');

  port = 0;
  GETBYTE (port, ',');
  GETBYTE (port, ')');

  s_in->sin_family = AF_INET;
  s_in->sin_port = htons (port);
  s_in->sin_addr.s_addr = htonl (ip);

  fd = socket (AF_INET, SOCK_STREAM, 0);
  if (fd == -1)
    {
      error ("socket: %s", strerror (errno));
      return -1;
    }

  if (connect (fd, (struct sockaddr*) s_in, sizeof (s_in[0])))
    {
      char addrbuf[MAX_ADDR_BUFSIZE];
      error ("can't connect to %s:%s",
	     addr2str (addrbuf, sizeof addrbuf,
		       (struct sockaddr*) s_in, sizeof (s_in[0])));
      return -1;
    }

  return fd;
 err:
  error ("%s: malformed 227 response", fc->dest->hostname);
  return -1;
}

static int
send_file (struct sockaddr_in *s_in, int ofd, int ifd)
{
  ssize_t nrd;
  char buf[BUFSIZ];

  while ((nrd = read (ifd, buf, sizeof buf)) > 0)
    {
      while (nrd)
	{
	  size_t nwr = write (ofd, buf, nrd);
	  if (nwr == 0)
	    {
	      error ("write error");
	      return -1;
	    }
	  else if (nwr == -1)
	    {
	      error ("write error: %s", strerror (errno));
	      return -1;
	    }
	  else
	    nrd -= nwr;
	}
    }
  if (nrd == -1)
    {
      error ("read error: %s", strerror (errno));
      return -1;
    }
  return 0;
}

static int
ftp_store_file (struct ftp_connection *fc, char const *name)
{
  struct sockaddr_in s_in;
  int fd, ofd;
  int rc;
  char const *dest_name;

  fd = open (name, O_RDONLY);
  if (fd == -1)
    {
      error ("can't open file %s: %s", name, strerror (errno));
      return -1;
    }

  do
    {
      if ((rc = ftp_send_command (fc, "PASV")) != 0)
	break;
      if ((rc = ftp_read_response (fc)) != 0)
	break;
      if (fc->code != 227)
	{
	  rc = -1;
	  error ("%s: unexpected response to PASV: %s",
		 fc->dest->hostname,
		 fc->buf);
	  break;
	}
      if ((ofd = ftp_parse_pasv_response (fc, &s_in)) == -1)
	{
	  rc = -1;
	  break;
	}

      dest_name = basename (name);
      if (verbose)
	debug ("uploading %s to ftp://%s/%s/%s", name,
	       fc->dest->hostname, fc->dest->upload_dir, dest_name);

      if ((rc = ftp_send_command (fc, "STOR %s/%s",
				  fc->dest->upload_dir, dest_name)) != 0)
	break;
      if ((rc = ftp_read_response (fc)) != 0)
	break;

      if (fc->code == 150)
	{
	  if ((rc = send_file (&s_in, ofd, fd)) != 0)
	    break;
	  close (ofd);
	  if ((rc = ftp_read_response (fc)) != 0)
	    break;
	  if (fc->code != 226)
	    {
	      rc = -1;
	      error ("%s: unexpected response to STOR: %s",
		     fc->dest->hostname,
		     fc->buf);
	      break;
	    }
	}
      else
	{
	  close (ofd);
	  rc = -1;
	  error ("%s: unexpected response to STOR: %s",
		 fc->dest->hostname,
		 fc->buf);
	  break;
	}
    }
  while (0);

  close (fd);
  return rc;
}

int
ftp_upload (struct destination *dest, int argc, char **argv)
{
  struct ftp_connection *fc;
  int rc;

  if (dest->hostname[0] == 0)
    {
      error ("empty hostname");
      return -1;
    }
  fc = ftp_connect (dest);
  if (ftp_read_response (fc))
    {
      ftp_disconnect (fc);
      return -1;
    }
  if (fc->code != 220)
    {
      error ("unrecognized response: %s", fc->buf);
      ftp_disconnect (fc);
      return -1;
    }

  if ((rc = ftp_auth (fc)) == 0)
    {
      int i;

      if ((rc = ftp_send_command (fc, "TYPE I")) != 0)
	goto err;
      if ((rc = ftp_read_response (fc)) != 0)
	goto err;
      if (fc->code != 200)
	{
	  error ("%s: cannot switch to binary mode: %s",
		 fc->dest->hostname, fc->buf);
	  rc = -1;
	  goto err;
	}

      for (i = 0; i < argc; i++)
	{
	  if ((rc = ftp_store_file (fc, argv[i])) != 0)
	    {
	      error ("failed to upload %s", argv[i]);
	      break;
	    }
	}
    err:
      ftp_close (fc);
    }
  else
    ftp_disconnect (fc);
  return rc;
}
