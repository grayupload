/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <limits.h>
#include <grayupload.h>

const char *progname;
static struct optdef *
option_find_short (struct parseopt *po, char **argptr)
{
  int i;
  struct optdef *found_opt = NULL;

  for (i = 0; i < po->optcount; i++)
    {
      struct optdef *opt = &po->optdef[po->optidx[i]];
      size_t namelen = strlen (opt->opt_name);

      if (namelen == 1 && opt->opt_name[0] == po->optname[0])
	{
	  while (opt > po->optdef && (opt->opt_flags & OPTFLAG_ALIAS))
	    opt--;
	  if (opt->opt_argdoc && po->optname[1])
	    {
	      *argptr = (char*) po->optname + 1;
	      po->opt_buf[0] = po->optname[0];
	      po->opt_buf[1] = 0;
	      po->optname = po->opt_buf;
	    }
	  else
	    *argptr = NULL;
	  return opt;
	}
    }

  error ("unrecognized option '-%c'", po->optname[0]);

  return NULL;
}

static struct optdef *
option_find_long (struct parseopt *po, char **argptr)
{
  size_t optlen = strcspn (po->optname, "=");
  int i;
  int found = 0;
  struct optdef *found_opt = NULL;

  for (i = 0; i < po->optcount; i++)
    {
      struct optdef *opt = &po->optdef[po->optidx[i]];
      size_t namelen = strlen (opt->opt_name);

      /* Ignore single-letter options when used with -- */
      if (namelen == 1)
	continue;

      if (optlen <= namelen)
	{
	  if (memcmp (opt->opt_name, po->optname, optlen) == 0)
	    {
	      switch (found)
		{
		case 0:
		  found_opt = opt;
		  found++;
		  if (optlen == namelen)
		    {
		      /* Exact match */
		      goto end;
		    }
		  break;

		case 1:
		  found++;
		  error ("option '--%*.*s' is ambiguous; possibilities:",
			 optlen, optlen, po->optname);
		  fprintf (stderr, "--%s\n", found_opt->opt_name);
		  /* fall through */
		case 2:
		  fprintf (stderr, "--%s\n", opt->opt_name);
		}
	    }
	  else if (found)
	    break;
	}
    }
 end:
  switch (found)
    {
    case 0:
      error ("unrecognized option '--%*.*s'", optlen, optlen, po->optname);
      break;

    case 1:
      while (found_opt > po->optdef && (found_opt->opt_flags & OPTFLAG_ALIAS))
	found_opt--;
      if (po->optname[optlen] == '=')
	*argptr = (char*) po->optname + optlen + 1;
      else
	*argptr = NULL;
      return found_opt;

    case 2:
      break;
    }
  return NULL;
}

static int header_col = 1;
static int opt_col = 2;
static int opt_doc_col = 29;
#define DEFAULT_RIGHT_MARGIN 79
static int rmargin = DEFAULT_RIGHT_MARGIN;

static void
detect_right_margin (void)
{
  struct winsize ws;
  unsigned r = 0;

  ws.ws_col = ws.ws_row = 0;
  if ((ioctl (1, TIOCGWINSZ, (char *) &ws) < 0) || ws.ws_col == 0)
    {
      char *p = getenv ("COLUMNS");
      if (p)
	{
	  unsigned long n;
	  char *ep;
	  errno = 0;
	  n = strtoul (p, &ep, 10);
	  if (!(errno || *ep || n > UINT_MAX))
	    r = n;
	}
      else
	r = DEFAULT_RIGHT_MARGIN;
    }
  else
    r = ws.ws_col;
  rmargin = r;
}

static int
is_ws (int c)
{
  return c == ' ' || c == '\t';
}

static void
format_para (int left, int right, int init_indent, char const *text, FILE *fp)
{
  int width = right - left - 1;

  if (strlen (text) < width)
    fprintf (fp, "%s\n", text);
  else
    {
      int len = 0;

      while (*text)
	{
	  size_t n = strcspn (text, " \t\n");
	  int para = 0;

	  if (len + n > width)
	    {
	      len = 0;
	      fprintf (fp, "\n%*.*s", left, left, "");
	    }

	  if (text[n] == '\n')
	    {
	      len = 0;
	      do
		{
		  n++;
		  len++;
		}
	      while (is_ws (text[n]));
	      len--;
	      if (!init_indent)
		para = 1;
	    }
	  else
	    len += n;

	  fwrite (text, n, 1, fp);
	  text += n;

	  while (is_ws (*text))
	    text++;

	  if (*text)
	    {
	      if (!para)
		fputc (' ', fp);
	      len++;
	    }
	}
      fputc ('\n', fp);
    }
}

static char const *dash_str[] = { "", "-", "--" };

static void
parseopt_list_options (struct parseopt *po, FILE *fp)
{
  struct optdef *opt;
  int prev_doc = 0;

  for (opt = po->optdef; opt->opt_name; )
    {
      if (opt->opt_flags & OPTFLAG_DOC)
	{
	  if (!prev_doc)
	    fputc ('\n', fp);
	  fprintf (fp, "%*.*s", header_col, header_col, "");
	  format_para (header_col, rmargin, 1, opt->opt_doc, fp);
	  fputc ('\n', fp);
	  opt++;
	  prev_doc = 1;
	}
      else if (opt->opt_flags & OPTFLAG_HIDDEN)
	opt++;
      else
	{
	  struct optdef *cur_opt = opt;
	  int col;

	  col = fprintf (fp, "%*.*s%s%s", opt_col, opt_col, "",
			 dash_str[strlen(cur_opt->opt_name) == 1 ? 1 : 2],
			 cur_opt->opt_name);
	  for (opt++; opt->opt_name && (opt->opt_flags & OPTFLAG_ALIAS); opt++)
	    {
	      col += fprintf (fp, ", %s%s",
			      dash_str[strlen(opt->opt_name) == 1 ? 1 : 2],
			      opt->opt_name);
	    }
	  if (cur_opt->opt_argdoc)
	    {
	      col += fprintf (fp, " %s", cur_opt->opt_argdoc);
	    }
	  if (col >= opt_doc_col)
	    {
	      fputc ('\n', fp);
	      col = 0;
	    }
	  for (; col < opt_doc_col; col++)
	    fputc (' ', fp);
	  format_para (opt_doc_col, rmargin, 1, cur_opt->opt_doc, fp);
	  prev_doc = 0;
	}
    }
}

int
parseopt_next (struct parseopt *po, char **ret_arg)
{
  char *arg, *opt_arg;
  struct optdef *opt;

  if (po->dash_count == 1 && po->optname && po->optname[1])
    po->optname++;
  else
    {
      if (po->argi == po->argc)
	return EOF;

      arg = po->argv[po->argi++];

      po->dash_count = 0;
      if (po->eopt)
	{
	  *ret_arg = arg;
	  return 0;
	}

      if (*arg == '-')
	{
	  arg++;
	  po->dash_count++;
	  if (*arg == '-')
	    {
	      arg++;
	      po->dash_count++;
	      if (*arg == 0)
		{
		  if (po->argi == po->argc)
		    return EOF;
		  po->eopt = 1;
		  *ret_arg = po->argv[po->argi++];
		  return 0;
		}
	    }
	  po->optname = arg;
	}
    }

  if (po->dash_count)
    {
      opt = (po->dash_count == 2 ? option_find_long : option_find_short)
	      (po, &opt_arg);
      if (!opt)
	{
	  exit (EX_USAGE);
	}
      if (opt->opt_argdoc)
	{
	  if (opt_arg)
	    *ret_arg = opt_arg;
	  else if (opt->opt_flags & OPTFLAG_ARG_OPTIONAL)
	    *ret_arg = NULL;
	  else if (po->argi == po->argc)
	    {
	      error ("option '%s%s' requires argument", dash_str[po->dash_count], opt->opt_name);
	      exit (EX_USAGE);
	    }
	  else
	    *ret_arg = po->argv[po->argi++];
	}
      else if (opt_arg)
	{
	  error ("option '%s%s' does not take argument", dash_str[po->dash_count], opt->opt_name);
	  exit (EX_USAGE);
	}

      return opt->opt_code;
    }
  else
    *ret_arg = arg;
  return 0;
}

int
parseopt_init (struct parseopt *po, int argc, char **argv)
{
  int i, j, k;

  if ((progname = strrchr (argv[0], '/')) == NULL)
    progname = argv[0];
  else
    progname++;

  po->argc = argc;
  po->argv = argv;
  po->argi = 1;
  po->eopt = 0;

  /* Collect and sort actual options */
  po->optcount = 0;
  for (i = 0; po->optdef[i].opt_name; i++)
    if (!(po->optdef[i].opt_flags & OPTFLAG_DOC))
      po->optcount++;

  if (po->optcount == 0)
    return 0;

  po->optidx = xcalloc (po->optcount, sizeof (po->optidx[0]));
  for (i = 0; po->optdef[i].opt_flags & OPTFLAG_DOC; i++)
    ;

  j = 0;
  po->optidx[j++] = i;
  if (po->optcount > 1)
    {
      for (i++; po->optdef[i].opt_name; i++)
	{
	  if (po->optdef[i].opt_flags & OPTFLAG_DOC)
	    continue;
	  po->optidx[j] = i;
	  for (k = j; k > 0 &&
		 strcmp (po->optdef[po->optidx[k-1]].opt_name,
			 po->optdef[po->optidx[k]].opt_name) > 0; k--)
	    {
	      int tmp = po->optidx[k];
	      po->optidx[k] = po->optidx[k-1];
	      po->optidx[k-1] = tmp;
	    }
	  j++;
	}
    }
  return 0;
}

static char extra_help[] =
  "Mandatory or optional arguments to long options are also mandatory or "
  "optional for any corresponding short options.\n\n"
  "A -- alone marks end of options: the remaining arguments are treated as "
  "file names.";

void
usage (int err, struct parseopt *po)
{
  FILE *fp = err ? stderr : stdout;
  detect_right_margin ();
  fprintf (fp, "usage: %s %s\n", progname, po->argdoc);
  format_para (0, rmargin, 0, po->descr, fp);
  parseopt_list_options (po, fp);
  fputc ('\n', fp);
  if (po->extradoc)
    {
      format_para (0, rmargin, 0, po->extradoc, fp);
      fputc ('\n', fp);
    }
  format_para (0, rmargin, 0, extra_help, fp);
  fputc ('\n', fp);
  fprintf (fp, "Report bugs and suggestions to <%s>\n", PACKAGE_BUGREPORT);
#ifdef PACKAGE_URL
  fprintf (fp, "%s home page: <%s>\n", PACKAGE_NAME, PACKAGE_URL);
#endif
  exit (err);
}
