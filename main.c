/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <sys/time.h>
#include <gpgme.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <regex.h>
#include <limits.h>
#include <ctype.h>
#include <grayupload.h>

void
verrorlog (char const *fmt, char *category, va_list ap)
{
  fprintf (stderr, "%s: ", progname);
  if (category)
    fprintf (stderr, "%s: ", category);
  vfprintf (stderr, fmt, ap);
  fputc ('\n', stderr);
}

void
error_at_locus (struct conf_locus *locus, char const *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  if (locus)
    {
      fprintf (stderr, "%s:%d: ", locus->file, locus->line);
      vfprintf (stderr, fmt, ap);
      va_end (ap);
      fputc ('\n', stderr);
    }
  else
    verrorlog (fmt, NULL, ap);
  va_end (ap);
}

void
error (char const *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  verrorlog (fmt, NULL, ap);
  va_end (ap);
}

void
warning (char const *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  verrorlog (fmt, "warning", ap);
  va_end (ap);
}

void
debug (char const *fmt, ...)
{
  va_list ap;
  va_start (ap, fmt);
  verrorlog (fmt, "debug", ap);
  va_end (ap);
}

void *
xmalloc (size_t size)
{
  void *p = malloc (size);
  if (!p)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }
  return p;
}

void *
xcalloc (size_t nmemb, size_t size)
{
  void *p = calloc (nmemb, size);
  if (!p)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }
  return p;
}

void *
xrealloc (void *ptr, size_t size)
{
  void *p = realloc (ptr, size);
  if (!p)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }
  return p;
}

char *
xstrdup (char const *s)
{
  char *p = strdup (s);
  if (!p)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }
  return p;
}

void *
x2nrealloc (void *p, size_t *pn, size_t s)
{
  size_t n = *pn;
  char *newp;

  if (!p)
    {
      if (!n)
	{
	  /* The approximate size to use for initial small
	     allocation requests, when the invoking code
	     specifies an old size of zero.  64 bytes is
	     the largest "small" request for the
	     GNU C library malloc.  */
	  enum { DEFAULT_MXFAST = 64 };

	  n = DEFAULT_MXFAST / s;
	  n += !n;
	}
    }
  else if ((size_t) -1 / 3 * 2 / s <= n)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }
  else
    n += (n + 1) / 2;

  newp = realloc (p, n * s);
  if (!newp)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }

  *pn = n;
  return newp;
}

int
vgetyn (const char *prompt, va_list ap)
{
  int state = 0;
  int c, resp;
  va_list aq;

  do
    {
      switch (state)
	{
	case 1:
	  if (c == ' ' || c == '\t')
	    continue;
	  resp = c;
	  state = 2;
	  /* fall through */
	case 2:
	  if (c == '\n')
	    {
	      switch (resp)
		{
		case 'y':
		case 'Y':
		  return 1;
		case 'n':
		case 'N':
		  return 0;
		default:
		  fprintf (stdout, "%s\n", "Please, reply 'y' or 'n'");
		}
	      /* fall through */
	    }
	  else
	    break;

	case 0:
	  va_copy (aq, ap);
	  vfprintf (stdout, prompt, aq);
	  va_end (aq);
	  fprintf (stdout, " [y/n]?");
	  fflush (stdout);
	  state = 1;
	  break;
	}
    } while ((c = getchar ()) != EOF);
  exit (EX_USAGE);
}

int
getyn (const char *prompt, ...)
{
  va_list ap;
  int rc;

  va_start (ap, prompt);
  rc = vgetyn (prompt, ap);
  va_end (ap);
  return rc;
}

struct string_entry *
string_entry_new (char const *s, size_t n)
{
  struct string_entry *ent = malloc (sizeof (*ent));
  if (!ent)
    {
      error ("out of memory");
      exit (EX_FAILURE);
    }
  ent->string = xmalloc (n + 1);
  memcpy (ent->string, s, n);
  ent->string[n] = 0;
  return ent;
}

void
string_list_append (STRING_LIST *slist, char const *s, size_t n)
{
  struct string_entry *ent = string_entry_new (s, n);
  SLIST_PUSH (slist, ent, next);
}

char *
string_list_coalesce (STRING_LIST *slist, char **pbuf, size_t *psize)
{
  char *buf = *pbuf;
  size_t size = *psize;
  size_t len = 0;

  while (!SLIST_EMPTY (slist))
    {
      struct string_entry *ent = SLIST_FIRST (slist);
      size_t n;
      SLIST_SHIFT (slist, next);
      n = strlen (ent->string);
      while (len + n + 1 > size)
	buf = x2nrealloc (buf, &size, 1);
      memcpy (buf + len, ent->string, n);
      len += n;
      free (ent->string);
      free (ent);
    }
  buf[len] = 0;
  *pbuf = buf;
  *psize = size;
  return buf;
}

void
string_list_free (STRING_LIST *slist)
{
  while (!SLIST_EMPTY (slist))
    {
      struct string_entry *ent = SLIST_FIRST (slist);
      SLIST_SHIFT (slist, next);
      free (ent->string);
      free (ent);
    }
}

enum
  {
    UPLOAD_LIST,
    DELETE_LIST,
    RMSYMLINK_LIST,
    SYMLINK_LIST,
    COMMENT_LIST,
    MAX__LIST
  };

STRING_LIST lists[MAX__LIST];

struct dest_trans
{
  char *s_exp;
  TRANSFORM xform;
};

/*
 * Translations for pseudo-URLs (destinations), supported by gnupload.
 */
static struct dest_trans stdtrans[] = {
  { "s%^([a-zA-Z_][a-zA-Z_-]+=)?([^@]+@)?download\\.gnu\\.org\\.ua:(alpha|ftp)/(.+)%"
    "\\1sftp://\\2download.gnu.org.ua/\\3;\\4%" },

  { "s%^([a-zA-Z_][a-zA-Z_-]+=)?([^@]+@)?puszcza\\.gnu\\.org\\.ua:(alpha|ftp)/(.+)%"
    "\\1sftp://\\2puszcza.gnu.org.ua:2222/\\3;\\4%" },

  { "s%^([a-zA-Z_][a-zA-Z_-]+=)?([^@]+@)?(alpha|ftp)\\.gnu\\.org:(.+)%"
    "\\1ftp://\\2ftp-upload.gnu.org/incoming/\\3;\\4%" },

  { "s%^([a-zA-Z_][a-zA-Z_-]+=)?(/[^;]+(;.+))?%"
    "\\1file://\\2%" },

  { NULL }
};

struct upload_method
{
  char *name;
  int (*upload) (struct destination *, int, char **);
};

static struct upload_method methods[] = {
  { "file", file_upload },
  { "sftp", sftp_upload },
  { "ftp",  ftp_upload },
  { NULL }
};

static struct upload_method *
find_upload_method (char const *str, size_t len)
{
  struct upload_method *meth;
  for (meth = methods; meth->name; meth++)
    if (strlen (meth->name) == len && memcmp (meth->name, str, len) == 0)
      return meth;
  return NULL;
}

static char *directory;

typedef SLIST_HEAD (, destination) DEST_HEAD;

static DEST_HEAD dest_head;

void
destination_free (struct destination *dest)
{
  free (dest->release_type);
  free (dest->url);
  free (dest->hostname);
  free (dest->username);
  free (dest->upload_dir);
  free (dest->directory);
  free (dest);
}

void
dest_list_free (DEST_HEAD *head)
{
  while (!SLIST_EMPTY (head))
    {
      struct destination *dp = SLIST_FIRST (head);
      SLIST_SHIFT (head, next);
      destination_free (dp);
    }
}

static char *
release_type_extract (char const **purl)
{
  char const *p;
  char *id;
  size_t len;

  p = *purl;
  if (isalpha (*p) || *p == '_')
    p++;
  else
    return NULL;
  for (; *p != '='; p++)
    {
      if (*p == 0 || !(isalnum (*p) || *p == '_' || *p == '-'))
	return NULL;
    }
  len = p - *purl;
  id = xmalloc (len + 1);
  memcpy (id, *purl, len);
  id[len] = 0;

  *purl = p + 1;
  return id;
}

static int
release_type_match (struct destination *dest, char const *type)
{
  if (dest->release_type == NULL)
    return 0;
  if (type == NULL)
    return 1;
  return strcmp (dest->release_type, type);
}

enum
  {
    VSEM_NONE = -1,
    VSEM_GNU,
    VSEM_KERNEL
  };

static int version_semantics = VSEM_NONE;

enum
  {
    STDRT_STABLE,
    STDRT_ALPHA
  };
static char *std_release_type[] = { "stable", "alpha" };

static char version_expr[] = "s/^.*-([0-9]+(\\.[0-9]+)+)\\.(tar|cpio)(\\..+)?$/\\1/";
static TRANSFORM version_xform;

static char const *
vsem_release_type_gnu (char const *version)
{
  char *minor;

  if ((minor = strchr (version, '.')) != NULL &&
      (minor = strchr (minor + 1, '.')) != NULL)
    {
      int len;

      minor++;
      len = strcspn (minor, ".");

      if (len > 1 && minor[0] == '9')
	return std_release_type[STDRT_ALPHA];
    }
  return std_release_type[STDRT_STABLE];

}

static char const *
vsem_release_type_kernel (char const *version)
{
  return std_release_type[strtoul (version, NULL, 10) % 2];
}

typedef struct
{
  char const *name;
  char const *(*derive) (char const *);
} VSEM;

static VSEM vsem_release_type[] = {
  [VSEM_GNU] = { "gnu", vsem_release_type_gnu },
  [VSEM_KERNEL] = { "kernel", vsem_release_type_kernel },
  { NULL }
};

static int
conf_set_vsem (char const *value, struct conf_keyword *kw,
	       struct conf_locus *loc)
{
  if (kw->dest != NULL)
    {
      int i;
      for (i = 0; vsem_release_type[i].name; i++)
	{
	  if (strcmp (vsem_release_type[i].name, value) == 0)
	    {
	      *(int*) kw->dest = i;
	      return 0;
	    }
	}
    }
  error_at_locus (loc, "unsupported version semantics: %s", value);
  return -1;
}

static char const *
file_name_to_release_type (char const *file_name)
{
  char *vs, *minor;
  char const *result;
  if (!version_xform)
    {
      version_xform = compile_transform_expr (version_expr, REG_EXTENDED);
      assert (version_xform != NULL);
    }
  vs = transform_string (version_xform, file_name);
  if (!vs || strcmp (vs, file_name) == 0)
    return NULL;
  result = vsem_release_type[version_semantics].derive (vs);
  free (vs);
  return result;
}

static char const *
derive_release_type (void)
{
  struct string_entry *se;
  char const *rt = NULL;

  if (version_semantics != VSEM_NONE)
    {
      SLIST_FOREACH (se, &lists[UPLOAD_LIST], next)
	{
	  char const *type = file_name_to_release_type (se->string);
	  if (type)
	    {
	      if (rt == NULL)
		rt = type;
	      else if (strcmp (rt, type) != 0)
		{
		  rt = NULL;
		  break;
		}
	    }
	}
    }
  return rt;
}

static int
url_push_to_head (char const *url, DEST_HEAD *head, struct conf_locus *loc)
{
  char const *arg = url;
  size_t len;
  char *p;
  struct destination *dest = xcalloc (1, sizeof (*dest));
  struct upload_method *meth = NULL;
  unsigned int port = 0;
  char *q, *end;

  /* Extract release type */
  dest->release_type = release_type_extract (&arg);
  dest->url = xstrdup (arg);

  /* See if we have scheme */
  if ((p = strchr (arg, ':')) != NULL && p[1] == '/' && p[2] == '/')
    {
      if ((meth = find_upload_method (arg, p - arg)) == NULL)
	{
	  error_at_locus (loc, "unsupported method: %s", url);
	  return -1;
	}
      arg = p + 3;
    }
  else
    {
      error_at_locus (loc, "%s: scheme is missing; did you mean --to?", url);
      return -1;
    }

  if ((p = strchr (arg, '@')) != NULL)
    {
      len = p - arg;
      dest->username = xmalloc (len + 1);
      memcpy (dest->username, arg, len);
      dest->username[len] = 0;
      arg = p + 1;
    }

  if ((p = strchr (arg, '/')) == NULL)
    {
      error_at_locus (loc, "%s: missing directory specification", url);
      return -1;
    }

  len = p - arg;

  if ((q = strchr (arg, ':')) != NULL)
    {
      len = q - arg;
      q++;
      errno = 0;
      port = strtoul (q, &end, 10);
      if (errno || end != p)
	{
	  struct servent *s;
	  char *buf;
	  size_t blen;

	  blen = p - q;
	  buf = xmalloc (blen + 1);
	  memcpy (buf, q, blen);
	  buf[blen] = 0;
	  s = getservbyname (buf, NULL);
	  free (buf);
	  if (s)
	    {
	      port = ntohs (s->s_port);
	    }
	  else
	    {
	      error_at_locus (loc, "%s: bad port specification", url);
	      return -1;
	    }
	}
    }

  dest->hostname = xmalloc (len + 1);
  memcpy (dest->hostname, arg, len);
  dest->hostname[len] = 0;
  dest->port = port;
  dest->upload = meth->upload;

  if (p > arg)
    p++;//FIXME

  len = strcspn (p, ";");
  dest->upload_dir = xmalloc (len + 1);
  memcpy (dest->upload_dir, p, len);
  dest->upload_dir[len] = 0;
  if (p[len] && p[len+1])
    dest->directory = xstrdup (p + len + 1);
  else if (directory)
    dest->directory = xstrdup (directory);
  else
    dest->directory = xstrdup (dest->upload_dir);

  SLIST_PUSH (head, dest, next);

  return 0;
}

static void
url_push (char const *url)
{
  if (url_push_to_head (url, &dest_head, NULL))
    exit (EX_USAGE);
}

static int
conf_url_push (char const *arg, struct conf_keyword *kw,
	       struct conf_locus *loc)
{
  DEST_HEAD tmp = SLIST_HEAD_INITIALIZER (tmp);
  DEST_HEAD *dest = kw->dest ? kw->dest : &tmp;
  int rc = url_push_to_head (arg, dest, loc);
  if (!SLIST_EMPTY (&tmp))
    dest_list_free (&tmp);
  return rc;
}

static int
dest_push_to_head (char const *to, DEST_HEAD *head, struct conf_locus *loc)
{
  struct dest_trans *trans;
  char *res = NULL;
  int rc;

  for (trans = stdtrans; trans->s_exp; trans++)
    {
      if (trans->xform == NULL)
	{
	  trans->xform = compile_transform_expr (trans->s_exp, REG_EXTENDED);
	  assert (trans->xform != NULL);
	}
      res = transform_string (trans->xform, to);
      if (res && strcmp (res, to))
	break;

      free (res);
      res = NULL;
    }

  if (res == NULL)
    {
      error_at_locus (loc, "unrecognized destination: %s", to);
      return -1;
    }

  if (verbose)
    debug ("%s translated to %s", to, res);
  rc = url_push_to_head (res, head, loc);
  free (res);

  return rc;
}

static void
dest_push (char const *to)
{
  if (dest_push_to_head (to, &dest_head, NULL))
    exit (EX_USAGE);
}

int verbose;
int dry_run;
int replace;
char *username;
TRANSFORM xform;
static char expr_latest[] = "s|-[0-9][0-9\\.]*\\(-[0-9][0-9]*\\)\\{0,1\\}\\.|-latest.|";

static int
conf_dest_push (char const *arg, struct conf_keyword *kw,
		struct conf_locus *loc)
{
  DEST_HEAD tmp = SLIST_HEAD_INITIALIZER (tmp);
  DEST_HEAD *dest = kw->dest ? kw->dest : &tmp;
  int rc = dest_push_to_head (arg, dest, loc);
  if (!SLIST_EMPTY (&tmp))
    dest_list_free (&tmp);
  return rc;
}

static int
conf_set_string (char const *value, struct conf_keyword *kw,
		 struct conf_locus *loc)
{
  if (kw->dest != NULL)
    {
      char **dest = kw->dest;
      *dest = xstrdup (value);
    }
  return 0;
}

static int
conf_set_int (char const *value, struct conf_keyword *kw,
	      struct conf_locus *loc)
{
  if (kw->dest != NULL)
    {
      unsigned long n;
      char *p;
      errno = 0;
      n = strtoul(value, &p, 10);
      if (*p)
	{
	  error_at_locus (loc, "bad value (near %s)", p);
	  return -1;
	}
      if (errno)
	{
	  error_at_locus (loc, "%s", strerror (errno));
	  return -1;
	}
      if (n > INT_MAX)
	{
	  error_at_locus (loc, "value out of range");
	  return -1;
	}
      *(int*)kw->dest = n;
    }
  return 0;
}

static int
conf_push_string_list (char const *arg, struct conf_keyword *kw,
		      struct conf_locus *loc)
{
  if (kw->dest != NULL)
    {
      string_list_append (kw->dest, arg, strlen (arg));
    }
  return 0;
}

static int
bool_value (char const *arg)
{
  static char const *v[2][4] = {
    { "off", "no", "false", "0" },
    { "on", "yes", "true", "1" }
  };
  int i, j;

  for (i = 0; i < sizeof (v) / sizeof (v[0]); i++)
    for (j = 0; j < sizeof (v[0]) / sizeof (v[0][0]); j++)
      if (strcmp (v[i][j], arg) == 0)
	return i;

  return -1;
}

static int
conf_set_bool (char const *arg, struct conf_keyword *kw,
	       struct conf_locus *loc)
{
  int b;

  if (bool_value (arg) == -1)
    {
      error_at_locus (loc, "not a valid boolean value");
      return -1;
    }
  if (kw->dest)
    *(int*)kw->dest = b;
  return 0;
}

static int
conf_set_latest (char const *arg, struct conf_keyword *kw,
		 struct conf_locus *loc)
{
  switch (bool_value (arg))
    {
    case 0:
      break;
    case 1:
      if (kw->dest)
	*(TRANSFORM*)kw->dest = compile_transform_expr (expr_latest, 0);
      break;
    default:
      error_at_locus (loc, "not a valid boolean value");
      return -1;
    }
  return 0;
}

static int
conf_set_transform (char const *arg, struct conf_keyword *kw,
		    struct conf_locus *loc)
{
  if (kw->dest)
    *(TRANSFORM*)kw->dest = compile_transform_expr (expr_latest, 0);
  return 0;
}

char const *
basename (char const *file)
{
  char *p = strrchr (file, '/');
  if (!p)
    return file;
  if (p[1] == 0)
    return NULL;
  return p + 1;
}

char *
catname (char const *dir, char const *file)
{
  size_t dirlen;
  char *ret = malloc ((dirlen = strlen (dir)) + strlen (file) + 2);
  if (ret)
    {
      strcpy (ret, dir);
      ret[dirlen] = '/';
      strcpy (ret + dirlen + 1, file);
    }
  return ret;
}

static void
show_file (char const *name, FILE *fp)
{
  int c;
  rewind (fp);
  debug ("file %s:", name);
  while ((c = fgetc (fp)) != EOF)
    fputc (c, stderr);
  debug ("end of %s", name);
}

FILE *
directive_open (struct destination *dest)
{
  FILE *fp;
  struct string_entry *se;

  fp = tmpfile ();
  if (!fp)
    {
      error ("can't create directive file: %s", strerror (errno));
      exit (EX_FAILURE);
    }

  fprintf (fp, "version: 1.2\n");
  fprintf (fp, "directory: %s\n", dest->directory ? dest->directory : dest->upload_dir);
  SLIST_FOREACH (se, &lists[COMMENT_LIST], next)
    fprintf (fp, "comment: %s\n", se->string);
  return fp;
}

#define GPG_ASSERT(code)						\
  do									\
    {									\
      int ec = code;							\
      if (ec != GPG_ERR_NO_ERROR)					\
	{								\
	  error ("%s: %s", #code, gpgme_strerror (ec));			\
	  exit (EX_FAILURE);						\
	}								\
    }									\
  while (0)

static gpgme_ctx_t
gpg_ctx_init (void)
{
  gpgme_ctx_t ctx;
  gpgme_error_t ec;
  int nkeys = 0;
  GPG_ASSERT (gpgme_new (&ctx));
  if (username)
    {
      ec = gpgme_op_keylist_start (ctx, username, 0);
      if (!ec)
	{
	  gpgme_key_t key;
	  while ((ec = gpgme_op_keylist_next (ctx, &key)) == 0)
	    {
	      ec = gpgme_signers_add (ctx, key);
	      gpgme_key_release (key);
	      nkeys++;
	    }
	}
      if (ec && gpg_err_code (ec) != GPG_ERR_EOF)
	{
	  error ("GPGME: Cannot list keys: %s"), gpgme_strerror (ec);
	  exit (EX_FAILURE);
	}
      if (nkeys == 0)
	{
	  error ("No secret keys found");
	  exit (EX_FAILURE);
	}
    }
  return ctx;
}

static void
clearsign (char const *filename, FILE *infile)
{
  gpgme_ctx_t ctx;
  gpgme_data_t din, dout;
  char buf[BUFSIZ];
  FILE *fp;
  size_t n;

  ctx = gpg_ctx_init ();

  rewind (infile);
  GPG_ASSERT (gpgme_data_new_from_stream (&din, infile));

  gpgme_set_textmode (ctx, 1);
  gpgme_set_armor (ctx, 1);

  GPG_ASSERT (gpgme_data_new (&dout));
  GPG_ASSERT (gpgme_op_sign (ctx, din, dout, GPGME_SIG_MODE_CLEAR));

  if (gpgme_data_seek (dout, 0, SEEK_SET) == -1)
    {
      error ("%s: %s", "gpgme_data_seek", strerror (errno));
      exit (EX_FAILURE);
    }

  fp = fopen (filename, "w");
  if (!fp)
    {
      error ("can't create %s: %s", filename, strerror (errno));
      exit (EX_FAILURE);
    }

  while ((n = gpgme_data_read (dout, buf, sizeof (buf))) > 0)
    {
      if (fwrite (buf, n, 1, fp) != 1)
	{
	  error ("error writing to %s: %s", filename, strerror (errno));
	  exit (EX_FAILURE);
	}
    }
  fclose (fp);
  gpgme_data_release (din);
  gpgme_data_release (dout);
  gpgme_release (ctx);
}

static char *
binsign (char const *filename)
{
  gpgme_ctx_t ctx;
  gpgme_data_t din, dout;
  char buf[BUFSIZ];
  FILE *fp;
  size_t n;
  static char suf[] = ".sig";
  char *signame;

  fp = fopen (filename, "r");
  if (!fp)
    {
      error ("can't open %s: %s", filename, strerror (errno));
      exit (EX_FAILURE);
    }

  ctx = gpg_ctx_init ();

  GPG_ASSERT (gpgme_data_new_from_stream (&din, fp));

  GPG_ASSERT (gpgme_data_new (&dout));
  GPG_ASSERT (gpgme_op_sign (ctx, din, dout, GPGME_SIG_MODE_DETACH));
  fclose (fp);

  if (gpgme_data_seek (dout, 0, SEEK_SET) == -1)
    {
      error ("%s: %s", "gpgme_data_seek", strerror (errno));
      exit (EX_FAILURE);
    }

  signame = xmalloc (strlen (filename) + sizeof (suf));
  strcpy (signame, filename);
  strcat (signame, suf);

  fp = fopen (signame, "w");
  if (!fp)
    {
      error ("can't create %s: %s", signame, strerror (errno));
      exit (EX_FAILURE);
    }

  while ((n = gpgme_data_read (dout, buf, sizeof (buf))) > 0)
    {
      if (fwrite (buf, n, 1, fp) != 1)
	{
	  error ("error writing to %s: %s", filename, strerror (errno));
	  exit (EX_FAILURE);
	}
    }
  fclose (fp);
  gpgme_data_release (din);
  gpgme_data_release (dout);
  gpgme_release (ctx);
  return signame;
}

static void
dest_upload_files (struct destination *dest, int argc, char **argv)
{
  if (dest->upload (dest, argc, argv))
    exit (EX_FAILURE);
}

static void
dest_upload (struct destination *dest)
{
  struct timeval tv;
  char namebuf[BUFSIZ];
  FILE *fp;
  struct string_entry *se;
  static char directive_suf[] = ".directive.asc";

  /* Process standalone directives */
  if (!SLIST_EMPTY (&lists[SYMLINK_LIST]) ||
      !SLIST_EMPTY (&lists[DELETE_LIST]) ||
      !SLIST_EMPTY (&lists[RMSYMLINK_LIST]))
    {
      size_t n;

      gettimeofday (&tv, NULL);
      namebuf[sizeof (namebuf) - 1] = 0;
      if (gethostname (namebuf, sizeof (namebuf) - 1))
	{
	  error ("cannot get my hostname: %s", strerror (errno));
	  exit (EX_FAILURE);
	}
      n = strlen (namebuf);
      if (n < sizeof (namebuf) - 1)
	snprintf (namebuf + n, sizeof (namebuf) - n - 1, "-%ld.%ld%s",
		  tv.tv_sec, tv.tv_usec, directive_suf);
      fp = directive_open (dest);
      for (se = SLIST_FIRST (&lists[SYMLINK_LIST]); se; se = SLIST_NEXT (se, next))
	{
	  fprintf (fp, "symlink: %s ", se->string);
	  se = SLIST_NEXT (se, next);
	  assert (se != NULL);
	  fprintf (fp, "%s\n", se->string);
	}

      SLIST_FOREACH (se, &lists[DELETE_LIST], next)
	{
	  fprintf (fp, "archive: %s\n", se->string);
	}

      SLIST_FOREACH (se, &lists[RMSYMLINK_LIST], next)
	{
	  fprintf (fp, "rmsymlink: %s\n", se->string);
	}

      if (verbose)
	debug ("created standalone directive file %s", namebuf);
      if (verbose > 1)
	show_file (namebuf, fp);

      clearsign (namebuf, fp);
      fclose (fp);

      if (!dry_run)
	{
	  char *fnames[1] = { namebuf };
	  dest_upload_files (dest, 1, fnames);
	}

      unlink (namebuf);
    }

  if (!SLIST_EMPTY (&lists[UPLOAD_LIST]))
    {
      struct string_entry *se;
      for (se = SLIST_FIRST (&lists[UPLOAD_LIST]); se; se = SLIST_NEXT (se, next))
	{
	  FILE *fp;
	  const char *bname; /* base upload file name */
	  char *dname = xmalloc (strlen (se->string) + sizeof (directive_suf));
	  char *triplet[3];
	  enum { N_DIR, N_FILE, N_SIG };

	  strcpy (dname, se->string);
	  strcat (dname, directive_suf);

	  triplet[N_DIR] = dname;
	  triplet[N_FILE] = (char*) se->string;
	  se = SLIST_NEXT (se, next);
	  assert (se != NULL);
	  triplet[N_SIG] = (char*) se->string;
	  bname = basename (triplet[N_FILE]);

	  if (verbose)
	    debug ("creating directive file %s", dname);

	  fp = directive_open (dest);
	  fprintf (fp, "filename: %s\n", bname);
	  if (replace)
	    fprintf (fp, "replace: true\n");

	  if (xform)
	    {
	      char *symlink = transform_string (xform, bname);
	      assert (symlink != NULL);
	      if (strcmp (symlink, bname))
		{
		  const char *sname;

		  fprintf (fp, "symlink: %s %s\n", bname, symlink);
		  free (symlink);

		  /*
		   * Note: the Automated FTP Uploads standard says that
		   * "The .sig file should not be explicitly mentioned in a
		   * [standalone] directive. When you specify a directive to
		   * operate on a file, its corresponding .sig file will be
		   * handled automatically (see subsection "FTP Upload
		   * Standalone Directives",
		   * https://www.gnu.org/prep/maintain/html_node/FTP-Upload-Standalone-Directives.html)
		   * However, this apparently does not hold for the symlink
		   * directory.  Hence the separate symlink directive for
		   * the sig file.
		   */

		  sname = basename (triplet[N_SIG]);
		  symlink = transform_string (xform, sname);
		  if (strcmp (symlink, triplet[N_SIG]))
		    fprintf (fp, "symlink: %s %s\n", sname, symlink);
		}
	      free (symlink);
	    }

	  if (verbose > 1)
	    show_file (dname, fp);
	  clearsign (dname, fp);
	  fclose (fp);

	  if (verbose)
	    debug ("uploading files to %s", dest->url);
	  if (!dry_run)
	    dest_upload_files (dest, 3, triplet);
	  unlink (dname);
	  free (dname);
	}
    }
}

enum
  {
    OPT_ARG = 0,
    OPT_TO,
    OPT_USER,
    OPT_HELP,
    OPT_DRY_RUN,
    OPT_VERBOSE,
    OPT_UPLOAD,
    OPT_DELETE,
    OPT_SYMLINK,
    OPT_RMSYMLINK,
    OPT_COMMENT,
    OPT_REPLACE,
    OPT_LATEST,
    OPT_XFORM,
    OPT_VERSION,
    OPT_DIRECTORY,
    OPT_URL,
    OPT_SYMLINK_REGEX,
    OPT_CONFIG,
    OPT_NO_CONFIG,
    OPT_RELEASE_TYPE,
  };

struct optdef options[] = {
  { "", NULL, OPTFLAG_DOC, 0, "Specifying destination." },
  { "", NULL, OPTFLAG_DOC, 0,
    "Destinations are defined using either --url or --to options. "
    "The --url option takes as its argument a URL in form\n\n"
    "   SCHEME://[USER@][HOST]/[UPLOAD_PATH][;DIRECTORY]\n\n"
    "where SCHEME is one of \"file\", \"ftp\", or \"sftp\". USER is "
    "the remote user name (in case it differs from the local one), HOST "
    "is the name of the remote host where to upload the files, UPLOAD_PATH "
    "is the pathname of the upload directory on host, and DIRECTORY is the "
    "destination directory on the remote filesystem. "
    "If SCHEME is \"file\", USER and HOST should be omitted. Otherwise, "
    "HOST must be present.\n\n"
    "The argument to the --to option is a destination specification "
    "compatible with the \"gnupload\" utility, i.e. one of:\n\n"
    "   alpha.gnu.org:DIRECTORY (via ftp)\n"
    "   ftp.gnu.org:DIRECTORY (via ftp)\n"
    "   download.gnu.org.ua:{alpha|ftp}/DIRECTORY (via sftp)\n"
    "   /DIRECTORY (file copy)\n\n"
    "Arguments to --url or --to can be prefixed with the release type "
    "name (TYPE=).  In this case, the corresponding destination will be "
    "used only if this type matches the one given with the --release-type "
    "option. This is normally used in configuration file.\n\n"
    "In both cases the -directory option can be used to supply alternative "
    "directory name."
  },
  { "to", "[TYPE=]TARGET:DIR", OPTFLAG_DEFAULT, OPT_TO,
    "Use built-in destination.  See above for the list of available "
    "TARGET:DIR pairs."},
  { "url", "[TYPE=]URL", OPTFLAG_DEFAULT, OPT_URL,
    "Upload to the supplied URL." },
  { "directory", "DIR", OPTFLAG_DEFAULT, OPT_DIRECTORY,
    "Set destination directory name." },
  { "t", "TYPE", OPTFLAG_DEFAULT, OPT_RELEASE_TYPE,
    "Select only destinations marked with this TYPE." },
  { "release-type", NULL, OPTFLAG_ALIAS },

  { "", NULL, OPTFLAG_DOC, 0, "General options" },
  { "n", NULL, OPTFLAG_DEFAULT, OPT_DRY_RUN,
    "Don't upload any files, just print what would have been done." },
  { "dry-run", NULL, OPTFLAG_ALIAS },
  { "u", "NAME", OPTFLAG_DEFAULT, OPT_USER,
    "Sign with key NAME." },
  { "user", NULL, OPTFLAG_ALIAS },
  { "v", NULL, OPTFLAG_DEFAULT, OPT_VERBOSE,
    "Increase verbosity." },
  { "verbose", NULL, OPTFLAG_ALIAS },
  { "comment", "TEXT", OPTFLAG_DEFAULT, OPT_COMMENT,
    "Supply comment for the upload. Multiple options are allowed." },
  { "no-config", NULL, OPTFLAG_DEFAULT, OPT_NO_CONFIG,
    "Don't read configuration file." },
  { "config", "FILE", OPTFLAG_DEFAULT, OPT_CONFIG,
    "Read configuration from FILE." },

  { "", NULL, OPTFLAG_DOC, 0, "File selection commands" },
  { "", NULL, OPTFLAG_DOC, 0, "The options --delete, --symlink, and --rmsymlink "
    "are standalone and are submitted in a separate directive "
    "file. Default is --upload. Use it explicitly, if "
    "you submit both standalone directive and upload files in one go. "
    "Note, that in this case the remote server gives no promise on the exact "
    "order in which the submitted directives will be applied." },
  { "delete", "FILE...", OPTFLAG_DEFAULT, OPT_DELETE,
    "Delete these files." },
  { "symlink", "FILE SYMLINK ...", OPTFLAG_DEFAULT, OPT_SYMLINK,
    "Create symbolic links.  Any number of FILE SYMLINK pairs can be given." },
  { "rmsymlink", "FILE...", OPTFLAG_DEFAULT, OPT_RMSYMLINK,
    "Remove symlinks." },
  { "upload", "FILE...", OPTFLAG_DEFAULT, OPT_UPLOAD,
    "Names of files to upload." },
  { "replace", NULL, OPTFLAG_DEFAULT, OPT_REPLACE,
    "Replace files on the target machine." },

  { "", NULL, OPTFLAG_DOC, 0, "Symlink creation options" },
  { "latest", NULL, OPTFLAG_DEFAULT, OPT_LATEST,
    "Create the \"-latest\" link for each uploaded file." },
  { "transform-symlink", "S-EXP", OPTFLAG_DEFAULT, OPT_XFORM,
    "For each uploaded file, create a symlink, with the name obtained by "
    "applying a sed expression S-EXP to the file name." },
  // For compatibility with gnupload
  { "symlink-regex", "[S-EXP]", OPTFLAG_ARG_OPTIONAL | OPTFLAG_HIDDEN,
    OPT_SYMLINK_REGEX,
    "same as -transform-symlink, if S-EXP is supplied, otherwise equivalent"
    " to -latest" },

  { "", NULL, OPTFLAG_DOC, 0, "Informative options" },
  { "?", NULL, OPTFLAG_DEFAULT, OPT_HELP,
    "Display this help list." },
  { "help", NULL, OPTFLAG_ALIAS },
  { "V", NULL, OPTFLAG_DEFAULT, OPT_VERSION,
    "Display program version." },
  { "version", NULL, OPTFLAG_ALIAS },

  { NULL }
};

struct parseopt po = {
  .descr = "Automated FTP Uploads",
  .argdoc = "[OPTIONS] [FILES]",
  .optdef = options,
  .extradoc = "Unless --no-config option is given, the program will look for "
  "file .grayupload in current working directory, and read it if it exists. "
  "The file contains, on each line, pairs keyword-value, delimited by "
  "whitespace.  Keywords correspond to long option names without -- and "
  "have the same meaning.  Allowed keywords are: to, url, directory, user, "
  "comment, verbose, latest, replace, and transform_symlink."
};

enum
  {
    CONF_TO,
    CONF_URL,
    CONF_DIRECTORY,
    CONF_USER,
    CONF_COMMENT,
    CONF_VERBOSE,
    CONF_LATEST,
    CONF_XFORM,
    CONF_REPLACE,
    CONF_VERSION_SEMANTICS,
  };

struct conf_keyword conftab[] = {
  [CONF_TO]        = { "to", conf_dest_push, &dest_head },
  [CONF_URL]       = { "url", conf_url_push, &dest_head },
  [CONF_DIRECTORY] = { "directory", conf_set_string, &directory },
  [CONF_USER]      = { "user", conf_set_string, &username },
  [CONF_COMMENT]   = { "comment", conf_push_string_list, &lists[COMMENT_LIST] },
  [CONF_VERBOSE]   = { "verbose", conf_set_int, &verbose },
  [CONF_LATEST]    = { "latest", conf_set_latest, &xform },
  [CONF_XFORM]     = { "transform_symlink", conf_set_transform, &xform },
  [CONF_REPLACE]   = { "replace", conf_set_bool, &replace },
  [CONF_VERSION_SEMANTICS] = { "version_semantics", conf_set_vsem, &version_semantics },
  { NULL }
};

static void
conf_keyword_disable (int idx)
{
  conftab[idx].dest = NULL;
}

static int copyright_year = 2024;

static void
version(void)
{
  printf("%s (%s) %s\n", progname, PACKAGE_NAME, PACKAGE_VERSION);
  printf("Copyright (C) 2023-%d Sergey Poznyakoff\n", copyright_year);
  printf("\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
");
}

static char user_agent_comment[] = "Uploaded by " PACKAGE_STRING "; " PACKAGE_URL ".";

#define DEFAULT_CONFIG_FILE_NAME ".grayupload"
static char *config_file_name = DEFAULT_CONFIG_FILE_NAME;

int
main (int argc, char **argv)
{
  int c;
  int list = UPLOAD_LIST;
  struct destination *dp;
  char *arg;
  char const *release_type = NULL;

  if (gpgme_check_version (MIN_GPGME_VERSION) == NULL)
    {
      error ("Failed to initialize GPGME (version less than "
	     MIN_GPGME_VERSION "?)");
      exit (EX_FAILURE);
    }

  string_list_append (&lists[COMMENT_LIST], user_agent_comment,
		      strlen (user_agent_comment));

  parseopt_init (&po, argc, argv);
  while ((c = parseopt_next (&po, &arg)) != EOF)
    {
      switch (c)
	{
	case OPT_ARG:
	  string_list_append (&lists[list], arg, strlen (arg));
	  break;

	case OPT_HELP:
	  usage (EX_OK, &po);
	  break;

	case OPT_VERSION:
	  version ();
	  exit (EX_OK);

	case OPT_VERBOSE:
	  conf_keyword_disable (CONF_VERBOSE);
	  verbose++;
	  break;

	case OPT_DRY_RUN:
	  dry_run = 1;
	  verbose++;
	  break;

	case OPT_USER:
	  username = arg;
	  break;

	case OPT_TO:
	  conf_keyword_disable (CONF_TO);
	  dest_push (arg);
	  break;

	case OPT_URL:
	  conf_keyword_disable (CONF_URL);
	  url_push (arg);
	  break;

	case OPT_RELEASE_TYPE:
	  release_type = arg;
	  break;

	case OPT_DIRECTORY:
	  conf_keyword_disable (CONF_DIRECTORY);
	  directory = arg;
	  break;

	case OPT_UPLOAD:
	  list = UPLOAD_LIST;
	  string_list_append (&lists[list], arg, strlen (arg));
	  break;

	case OPT_DELETE:
	  list = DELETE_LIST;
	  string_list_append (&lists[list], arg, strlen (arg));
	  break;

	case OPT_SYMLINK:
	  list = SYMLINK_LIST;
	  string_list_append (&lists[list], arg, strlen (arg));
	  break;

	case OPT_RMSYMLINK:
	  list = RMSYMLINK_LIST;
	  string_list_append (&lists[list], arg, strlen (arg));
	  break;

	case OPT_COMMENT:
	  list = COMMENT_LIST;
	  string_list_append (&lists[list], arg, strlen (arg));
	  break;

	case OPT_REPLACE:
	  conf_keyword_disable (CONF_REPLACE);
	  replace = 1;
	  break;

	case OPT_LATEST:
	  conf_keyword_disable (CONF_LATEST);
	  xform = compile_transform_expr (expr_latest, 0);
	  break;

	case OPT_XFORM:
	  conf_keyword_disable (CONF_XFORM);
	  xform = compile_transform_expr (arg, 0);
	  break;

	case OPT_SYMLINK_REGEX:
	  if (arg)
	    xform = compile_transform_expr (arg, 0);
	  else
	    xform = compile_transform_expr (expr_latest, 0);
	  break;

	case OPT_CONFIG:
	  config_file_name = arg;
	  break;

	case OPT_NO_CONFIG:
	  config_file_name = NULL;
	  break;

	default:
	  error ("unrecognized option: %s", po.optname);
	  exit (EX_USAGE);
	}
    }

  if (config_file_name &&
      config_parse (config_file_name, conftab,
		    strcmp (config_file_name, DEFAULT_CONFIG_FILE_NAME) == 0))
    exit (EX_CONFIG);

  if (!release_type)
    release_type = derive_release_type ();

  if (SLIST_EMPTY (&dest_head))
    {
      error ("no destination given");
      usage (EX_USAGE, &po);
    }
  else
    {
      DEST_HEAD dh = SLIST_HEAD_INITIALIZER (dh);
      while (!SLIST_EMPTY (&dest_head))
	{
	  struct destination *dp = SLIST_FIRST (&dest_head);
	  SLIST_SHIFT (&dest_head, next);
	  if (release_type_match (dp, release_type) == 0)
	    SLIST_PUSH (&dh, dp, next);
	  else
	    destination_free (dp);
	}

      if (SLIST_EMPTY (&dh))
	{
	  if (release_type)
	    error ("no destination matches release type %s", release_type);
	  else
	    error ("no matching destination; use --release-type option");
	  exit (EX_USAGE);
	}

      dest_head = dh;
    }

  if (SLIST_EMPTY (&lists[UPLOAD_LIST]) &&
      SLIST_EMPTY (&lists[DELETE_LIST]) &&
      SLIST_EMPTY (&lists[RMSYMLINK_LIST]) &&
      SLIST_EMPTY (&lists[SYMLINK_LIST]))
    {
      error ("nothing to do");
      usage (EX_USAGE, &po);
    }

  if (!SLIST_EMPTY (&lists[SYMLINK_LIST]))
    {
      struct string_entry *ent;
      int i = 0;
      SLIST_FOREACH (ent, &lists[SYMLINK_LIST], next)
	{
	  i = !i;
	}
      if (i)
	{
	  error ("--symlink requires even number of arguments");
	  exit (EX_USAGE);
	}
    }

  if (!SLIST_EMPTY (&lists[UPLOAD_LIST]))
    {
      struct string_entry *se;

      if (verbose)
	debug ("Signing files");

      for (se = SLIST_FIRST (&lists[UPLOAD_LIST]); se; )
	{
	  struct string_entry *next = SLIST_NEXT (se, next);
	  struct string_entry *sigent;
	  char *signame = binsign (se->string);
	  sigent = xmalloc (sizeof (*sigent));
	  sigent->string = signame;
	  SLIST_NEXT (sigent, next) = next;
	  SLIST_NEXT (se, next) = sigent;
	  se = next;
	}
    }

  SLIST_FOREACH (dp, &dest_head, next)
    {
      dest_upload (dp);
    }

  exit (EX_OK);
}
