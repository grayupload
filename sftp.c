/*
 * grayupload - implementation of the GNU Automatic FTP Uploads
 * Copyright (C) 2023-2024 Sergey Poznyakoff
 *
 * Grayupload is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * Grayupload is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with grayupload. If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <libssh/libssh.h>
#include <libssh/sftp.h>
#include <grayupload.h>

int
verify_knownhost (ssh_session session)
{
  enum ssh_known_hosts_e state;
  unsigned char *hash = NULL;
  ssh_key srv_pubkey = NULL;
  size_t hlen;
  char *hexa;
  int rc;

  rc = ssh_get_server_publickey (session, &srv_pubkey);
  if (rc < 0)
    return -1;

  rc = ssh_get_publickey_hash (srv_pubkey,
			       SSH_PUBLICKEY_HASH_SHA256,
			       &hash,
			       &hlen);
  ssh_key_free (srv_pubkey);
  if (rc < 0)
    return -1;

  state = ssh_session_is_known_server (session);
  switch (state)
    {
    case SSH_KNOWN_HOSTS_OK:
      /* OK */
      break;

    case SSH_KNOWN_HOSTS_CHANGED:
      error ("Host key for server changed: it is now:");
      ssh_print_hash (SSH_PUBLICKEY_HASH_SHA256, hash, hlen);
      error ("Connection aborted");
      ssh_clean_pubkey_hash (&hash);
      return -1;

    case SSH_KNOWN_HOSTS_OTHER:
      error ("The host key for this server was not found but an other "
	     "type of key exists.");
      error ("An attacker might change the default server key to"
	     "confuse your client into thinking the key does not exist.");
      ssh_clean_pubkey_hash (&hash);
      return -1;

    case SSH_KNOWN_HOSTS_NOT_FOUND:
      error ("Could not find known host file.");
      error ("If you accept the host key here, the file will be automatically created");
      /* fall through */
    case SSH_KNOWN_HOSTS_UNKNOWN:
      hexa = ssh_get_hexa (hash, hlen);
      fprintf (stderr, "The server is unknown. Public key hash: %s\n", hexa);
      ssh_string_free_char (hexa);
      ssh_clean_pubkey_hash (&hash);
      if (!getyn ("Do you trust this key?"))
	return -1;

      rc = ssh_session_update_known_hosts (session);
      if (rc < 0)
	{
	  error ("%s", strerror (errno));
	  return -1;
	}
      break;

    case SSH_KNOWN_HOSTS_ERROR:
      error ("%s", ssh_get_error (session));
      ssh_clean_pubkey_hash (&hash);
      return -1;
    }

  ssh_clean_pubkey_hash (&hash);
  return 0;
}

int
sftp_upload_file (sftp_session sftp, ssh_session session,
		  struct destination *dest, char const *infile)
{
  char const *name;
  char *outname;
  FILE *fp;
  char buf[BUFSIZ];
  size_t nread, nwritten;
  sftp_file file;
  int rc;

  if ((name = basename (infile)) == NULL)
    {
      error ("%s: bad file name", infile);
      return SSH_ERROR;
    }

  fp = fopen (infile, "r");
  if (!fp)
    {
      error ("%s: can't open: %s", infile, strerror (errno));
      return SSH_ERROR;
    }

  if (dest->upload_dir)
    outname = catname (dest->upload_dir, name);
  else
    outname = strdup (name);
  if (!outname)
    return SSH_ERROR;

  if (verbose)
    debug ("uploading %s => sftp://%s/%s", infile, dest->hostname, outname);
  file = sftp_open (sftp, outname, O_WRONLY | O_CREAT | O_TRUNC, 0644);
  if (file == NULL)
    {
      error ("%s: can't open file for writing: %s",
	     outname, ssh_get_error (session));
      free (outname);
      fclose (fp);
      return SSH_ERROR;
    }

  rc = SSH_OK;
  while ((nread = fread (buf, 1, sizeof (buf), fp)) != 0)
    {
      nwritten = sftp_write (file, buf, nread);
      if (nwritten != nread)
	{
	  error ("%s: can't write data to file: %s",
		 outname, ssh_get_error(session));
	  rc = SSH_ERROR;
	  break;
	}
    }
  if (rc == SSH_OK && ferror (fp))
    {
      error ("%s: read error: %s", outname, strerror (errno));
      rc = SSH_ERROR;
    }
  sftp_close (file);
  fclose (fp);
  free (outname);
  return rc;
}

int
do_sftp_upload (ssh_session session, struct destination *dest, int argc, char **argv)
{
  sftp_session sftp;
  int rc;
  int i;

  sftp = sftp_new (session);
  if (sftp == NULL)
    {
      error ("error allocating SFTP session: %s", ssh_get_error (session));
      return SSH_ERROR;
    }

  rc = sftp_init (sftp);
  if (rc != SSH_OK)
    {
      error ("error initializing SFTP session: code %d",
	     sftp_get_error (sftp));
      sftp_free (sftp);
      return rc;
    }

  for (i = 0; i < argc; i++)
    {
      if ((rc = sftp_upload_file (sftp, session, dest, argv[i])) != SSH_OK)
	break;
    }

  sftp_free (sftp);
  return rc;
}

int
sftp_upload (struct destination *dest, int argc, char **argv)
{
  ssh_session session;
  int rc;

  if (dest->hostname[0] == 0)
    {
      error ("empty hostname");
      return -1;
    }

  session = ssh_new ();
  if (session == NULL)
    return -1;
  ssh_options_set (session, SSH_OPTIONS_HOST, dest->hostname);
  if (dest->port != 0)
    {
      unsigned int n = dest->port;
      ssh_options_set (session, SSH_OPTIONS_PORT, &n);
    }
  if (verbose > 2)
    {
      int verbosity = SSH_LOG_DEBUG;
      ssh_options_set (session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
    }

  rc = ssh_connect (session);
  if (rc != SSH_OK)
    {
      error ("error connecting to %s: %s",
	     dest->hostname, ssh_get_error (session));
      ssh_free (session);
      return -1;
    }

  if ((rc = verify_knownhost (session)) == 0)
    {
      rc = ssh_userauth_publickey_auto (session, dest->username, NULL);
      switch (rc)
	{
	  case SSH_AUTH_SUCCESS:
	    if ((rc = do_sftp_upload (session, dest, argc, argv)) != SSH_OK)
	      {
		error ("sftp upload failed");
	      }
	    break;

	case SSH_AUTH_ERROR:
	  error ("%s: authentication failed: %s", dest->hostname,
		 ssh_get_error (session));
	  break;

	case SSH_AUTH_DENIED:
	  error ("%s: authentication denied", dest->hostname);
	  break;

	case SSH_AUTH_PARTIAL:
	  error ("%s: authentication succeeded only partially; you have to use another method",
		 dest->hostname);
	  break;

	default:
	  error ("%s: authentication failed", dest->hostname);
	  break;
	}
    }

  ssh_disconnect (session);
  ssh_free (session);
  return rc;
}
